
source ~yesw2000/work/Users/Walter/setup.sh
cd ../
wget http://madgraph.physics.illinois.edu/Downloads/MG5_aMC_v2.6.1.tar.gz

tar -zxf MG5_aMC_v2.6.1.tar.gz
rm MG5_aMC_v2.6.1.tar.gz

cd MG5_aMC_v2_6_1

printf "\ninstall pythia8\nquit\n"|./bin/mg5_aMC

printf "\ninstall Delphes\nquit\n"|./bin/mg5_aMC

sed -i 's/# automatic_html_opening = True/automatic_html_opening = False/' input/mg5_configuration.txt
sed -i 's/# run_mode = 2/run_mode = 0/' input/mg5_configuration.txt
sed -i 's/!partonlevel:mpi = off/partonlevel:mpi = off/' Template/LO/Cards/pythia8_card_default.dat

cd ../MadGraphCondor
