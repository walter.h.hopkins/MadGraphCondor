#!/bin/env python

import math, sys, glob, os, shutil, subprocess, socket, errno, argparse

headers = {
    "top1Lep":("set group_subprocesses Auto\n"
               "set ignore_six_quark_processes False\n"
               "set loop_optimized_output True\n"
               "set loop_color_flows False\n"
               "set gauge unitary\n"
               "set complex_mass_scheme False\n"
               "set max_npoint_for_channel 0\n"
               "import model sm\n"
               #"define p = g u c d s u~ c~ d~ s~\n"
               "define j = g u c d s u~ c~ d~ s~\n"
               "define l+ = e+ mu+ ta+\n"
               "define l- = e- mu- ta-\n"
               "define vl = ve vm vt\n"
               "define vl~ = ve~ vm~ vt~\n"
               "define w = w+ w-\n"
               "define l = l+ l-\n"
               "define vvl = vl vl~\n"

               #"define p = g u c d s u~ c~ d~ s~ b b~\n"
               "generate p p > t t~ > w+ w- b b~, (w+ > l vvl), ( w- > j j)\n"
               "add process p p > t t~ > w+ w- b b~, (w- > l vvl), ( w+ > j j)\n"
    ),

    "top1Lep2ExtraJets":("set group_subprocesses Auto\n"
               "set ignore_six_quark_processes False\n"
               "set loop_optimized_output True\n"
               "set loop_color_flows False\n"
               "set gauge unitary\n"
               "set complex_mass_scheme False\n"
               "set max_npoint_for_channel 0\n"
               "import model sm\n"
               #"define p = g u c d s u~ c~ d~ s~\n"
               "define j = g u c d s u~ c~ d~ s~\n"
               "define l+ = e+ mu+ ta+\n"
               "define l- = e- mu- ta-\n"
               "define vl = ve vm vt\n"
               "define vl~ = ve~ vm~ vt~\n"
               "define w = w+ w-\n"
               "define l = l+ l-\n"
               "define vvl = vl vl~\n"

               #"define p = g u c d s u~ c~ d~ s~ b b~\n"
               "generate p p > t t~ > (t > W+ b, W+ > j j) w+ w- b b~, (w+ > l vvl), ( w- > j j) @1\n"
               "add process p p > t t~ > w+ w- b b~, (w- > l vvl), ( w+ > j j) @2\n"
                         
               "add process p p > t t~ j > w+ w- b b~ j, (w+ > l vvl), ( w- > j j) @3\n"
               "add process p p > t t~ j > w+ w- b b~ j, (w- > l vvl), ( w+ > j j) @4\n"
                         
               "add process p p > t t~ j j > w+ w- b b~ j j, (w+ > l vvl), ( w- > j j) @5\n"
               "add process p p > t t~ j j > w+ w- b b~ j j, (w- > l vvl), ( w+ > j j) @6\n"

                         
    ),
    
    "zNuNuNp0":(
                "import model sm-no_b_mass\n"       
                "generate p p > vl vl~\n"
    ),
    
    "zNuNuNp1":(
                "import model sm-no_b_mass\n"
                "generate p p > vl vl~ j\n"
    ),
    
    "zNuNuNp2":(
                "import model sm-no_b_mass\n"
                "generate p p > vl vl~ j j\n"
    ),
    
    "zNuNuNp3":(
                "import model sm-no_b_mass\n"
                "generate p p > vl vl~ j j j \n"
    ),

    "zNuNuNp4":(
                "import model sm-no_b_mass\n"
                "generate p p > vl vl~ j j j j\n"
    ),

    "zNuNuNp45":(
                "import model sm-no_b_mass\n"
                "generate p p > vl vl~ j j j j @4\n"
        "add process p p > vl vl~ j j j j j @5\n"
    ),
    "stop":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~ @0\n",
    "stopLeft":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~\n",
    "stopLeftMadspin":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~\n",
    "stopRight":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~\n",
    "stopGravitino":"import model sm\nimport model GldGrv_UFO\ngenerate p p > t1 t1~\n",
    "stopGravitinoLeft":"import model sm\nimport model GldGrv_UFO\ngenerate p p > t1 t1~\n",
    "stopGravitinoRight":"import model sm\nimport model GldGrv_UFO\ngenerate p p > t1 t1~\n",
    
    "stop1J":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~ @0\nadd process p p > t1 t1~ j @1\n",
    "stop2J":"import model sm\nimport model MSSM_SLHA2\ngenerate p p > t1 t1~ @0\nadd process p p > t1 t1~ j @1\nadd process p p > t1 t1~ j j @2\n",
    "stopGetWidth":"import model sm\nimport model MSSM_SLHA2\ndefine l = e+ e- mu+ mu- ta+ ta- vl vl~\ngenerate t1 > b n1 l l\n",
    "stopDilep":"import model sm\nimport model MSSM_SLHA2\ndefine susy = x1+ x1- x2+ x2- n2 n3 n4 t2 t2~ b1 b1~ b2 b2~\ndefine l = e+ e- mu+ mu- ta+ ta- vl vl~\ngenerate p p > t1 t1~, t1 > b n1 l l / susy, t1~ > b~ n1 l l / susy\n",
    "hh4b":"import model sm\ngenerate p p > h h, (h > b b~), (h > b b~)",
    "dijet":"import model sm\ngenerate p p > j j",
}

def submitOneSamp(nEvts, process, nEvtsPerNode=50000, totalNodes=100, topMass=173, workDirBase="/usatlas/groups/bnl_local2/whopkins/deepMLSamps/", sqrtS=13, basePath="/usatlas/u/whopkins/bkgGen/", seedStart=1, stopMass=-1, lspMass=-1, preselection="set ptj 20.0\nset pta 10.0\nset ptl 6.0\nset misset 200.0\n", skimDelphes=True, rmDelphes=True, rmLHE=True, width='', getIDStrOnly=False):
    # Maximum number of events allowed when running with pythia
    maxEvtsPerNode = 50000

    print rmDelphes, "we are going to delete delphes files"
    if nEvtsPerNode > maxEvtsPerNode:
        print "You can't run more than 50K events per run. Setting events per node to", maxEvtsPerNode
        nEvtsPerNode = maxEvtsPerNode

    if nEvtsPerNode > nEvts:
        nEvtsPerNode = nEvts

    nNodes = int(math.ceil((nEvts*1.0)/nEvtsPerNode))
    # Write proc_cards and executable
    idStr = "%s_%dTeV_seedStart%d" % (process, sqrtS, seedStart)
    if "stop" in process:
        idStr = "%s_%.2f_%.2f_%dTeV_seedStart%d" % (process, stopMass, lspMass,sqrtS, seedStart)

    if getIDStrOnly:
        return idStr
    execFileName = "executables/genEvts_"+idStr+".sh"
    execFile = open(execFileName, "w")
    execFileStr = (
        "#!/bin/bash\n"
        "$4/tmp/$2_$3/bin/madevent $1/MadGraphCondor/cards/run_card_$2_$3.dat\n"
        "mv $4/tmp/$2_$3/Events/run_01/unweighted_events.lhe.gz $4/submit/$2/lhe/$2_$3.lhe.gz\n"
        "mv $4/tmp/$2_$3/Events/run_01/run_01_tag_1_banner.txt $4/submit/$2/banner/$2_$3_banner.txt\n\n"
    )

    if "GetWidth" not in process:
        if skimDelphes:
            execFileStr+="python $1/MadGraphCondor/skimSlimDelphes.py $4/tmp/$2_$3/Events/run_01/tag_1_delphes_events.root $4/submit/$2/skimSlim/$2_$3.root $1/MG5_aMC_v2_6_1/Delphes/ $4/submit/$2/banner/$2_$3_banner.txt\n"
        else:
            execFileStr+="python $1/MadGraphCondor/addXSec.py $4/tmp/$2_$3/Events/run_01/tag_1_delphes_events.root $1/MG5_aMC_v2_6_1/Delphes/ $4/submit/$2/banner/$2_$3_banner.txt\n"
            
        execFileStr += "mv $4/tmp/$2_$3/Events/run_01/tag_1_delphes_events.root $4/submit/$2/delphes/$2_$3.root\n"

        if rmDelphes:
            execFileStr+="rm $4/submit/$2/delphes/$2_$3.root\n"
        if rmLHE:
            "rm $4/submit/$2/lhe/$2_$3.lhe.gz\n"
        
    execFileStr += "rm -rf $4/tmp/$2_$3\n\n"
        
    execFile.write(execFileStr)
    execFile.close()
    print idStr
    workDir = workDirBase+"/tmp/"+idStr
    print "workDir=", workDir

    outDir = workDirBase+"/submit/"
    print "outDir=", outDir
    outFNameBase = idStr
    procCardHeader = headers[process]+"output "+workDir
    print "procCardHeader=\n",procCardHeader

    baseCardName = "BaseParamCard"
    bodyStr = ""
    if stopMass < topMass+lspMass/1000:
        bodyStr = "_three_body"
    lspType = 'bino'
    if 'stopGravitino' in process:
        lspType = 'gravitino'
                  
    handedness = 'left'
    if 'Right' in process:
        handedness = 'right'
    baseCardName += bodyStr+".dat"

    procF = open("cards/proc_card_"+idStr+".dat", 'w');
    procF.write(procCardHeader+"_"+str(seedStart)+"\n")
    procF.close();
    os.system("../MG5_aMC_v2_6_1/bin/mg5_aMC cards/proc_card_"+idStr+".dat")
    for nodeI in range(seedStart, seedStart+nNodes):
        # Write proc card
        if nodeI !=  seedStart:
            print "Copying", workDir+"_"+str(seedStart), workDir+"_"+str(nodeI)
            shutil.copytree(workDir+"_"+str(seedStart), workDir+"_"+str(nodeI))
            
        # Write run card
        runF = open("cards/run_card_"+idStr+"_"+str(nodeI)+".dat", 'w');
        runF.write("generate_events\n")
        if "GetWidth" not in process:
            runF.write("delphes=ON\n");
        runF.write("set lhc "+str(sqrtS)+"\n")
        if "stop" in process:
            runF.write(basePath+"MadGraphCondor/"+baseCardName+"\n")
        if "GetWidth" not in process:
            runF.write(basePath+"MadGraphCondor/delphes_card_ATLAS.tcl\nset iseed "+str(nodeI)+"\n")
        runF.write("set nevents "+str(nEvtsPerNode)+"\n")
        if "stop" in process:
            runF.write("set mass 1000006 "+str(stopMass)+"\n");
            #if width != "":
            #    runF.write("set width 1000006 "+width+"\n")
            if "GetWidth" in process:
                runF.write("set mass 1000022 1\n")
            else:
                if 'stopGravitino' in process:
                    runF.write("set mass 1000039 "+str(lspMass/1000.)+"\n")
                    runF.write("set mass 1000049 "+str(lspMass/1000.)+"\n")
                else:
                    runF.write("set mass 1000022 "+str(lspMass/1000.)+"\n")
        runF.write("set mass 6 "+str(topMass)+"\n")
        if preselection != "":
            runF.write(preselection)
        runF.write("0\n")
        
        runF.close();

    # Now write condor submit file
    condorHeader = ("Universe = vanilla\n"
                    "Executable = "+execFileName+"\n"
                    "Error = "+outDir+'condor/'+idStr+"/error/job.$(Process).error\n"
                    "Output = "+outDir+'condor/'+idStr+"/out/job.$(Process).out\n"
                    "Log = "+outDir+'condor/'+idStr+"/log/job.$(Process).log\n\n"

                    "should_transfer_files = YES\n"
                    "when_to_transfer_output = ON_Exit\n"
                    "transfer_output = True\n"
                    "getenv = true\n"
                    #"transfer_input_files = cards,genEvts.sh\n\n"
                    "transfer_input_files = "+execFileName+"\n\n"
                    )
    condorStr = condorHeader;
    
    if "spar" in socket.gethostname() or "acas" in socket.gethostname():
        condorStr += "\naccounting_group = group_atlas.oregon\n"
    #elif "usatlas" in socket.gethostname():
    #    condorStr += "\n+ProjectName=\"atlas.org.uoregon\"\n"
                
    for nodeI in range(seedStart, seedStart+nNodes):
        #condorStr += "Arguments = "+workDir+"_"+str(nodeI)+" "+basePath+" "+basePath+"MadGraphCondor/cards/proc_card_"+idStr+"_"+str(nodeI)+".dat "+basePath+"MadGraphCondor/cards/run_card_"+idStr+"_"+str(nodeI)+".dat "+outDir+" "+outFNameBase+" "+outFNameBase+"_"+str(nodeI)+"\nQueue\n\n";
        condorStr += "Arguments = "+basePath+" "+idStr+" "+str(nodeI)+" "+workDirBase+"\nQueue\n\n";

    # Now delete the old logs
    print "Deleting old logs if they exist in", outDir+'condor/'+idStr 
    if os.path.exists(outDir+'condor/'+idStr):
        shutil.rmtree(outDir+'condor/'+idStr)
    print "Done deleting old logs. Creating new log directories."
    os.mkdir(outDir+'condor/'+idStr)
    os.mkdir(outDir+'condor/'+idStr+'/error')
    os.mkdir(outDir+'condor/'+idStr+'/log')
    os.mkdir(outDir+'condor/'+idStr+'/out')
    os.mkdir(outDir+'condor/'+idStr+'/submit')

    condorF = open(outDir+'condor/'+idStr+'/submit/condorSubmit_'+idStr, 'w');
    condorF.write(condorStr);
    condorF.close();
    
    # Now clean up any files from a previous submit
    print "Checking if", outDir+'/'+outFNameBase, "exists, if so deleting it"
    if os.path.exists(outDir+'/'+outFNameBase):
        print "Deleting",outDir+'/'+outFNameBase
        shutil.rmtree(outDir+'/'+outFNameBase)
    
    print "Done deleting. Making", outDir+'/'+outFNameBase
    os.mkdir(outDir+'/'+outFNameBase)
    os.mkdir(outDir+'/'+outFNameBase+'/delphes')
    os.mkdir(outDir+'/'+outFNameBase+'/lhe')
    os.mkdir(outDir+'/'+outFNameBase+'/banner')
    os.mkdir(outDir+'/'+outFNameBase+'/skimSlim')

    print "condor_submit "+outDir+'condor/'+idStr+"/submit/condorSubmit_"+idStr
    
    os.system("condor_submit "+outDir+'condor/'+idStr+"/submit/condorSubmit_"+idStr);
    return idStr

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Submit Madgraph jobs to condor')
    parser.add_argument('process', type=str, help='Process to generate', default="stop")
    parser.add_argument('--stopMass', type=int, help='Stop mass in GeV', default=600)
    parser.add_argument('--lspMass', type=int, help='LSP mass in MeV', default=300000)
    parser.add_argument('--seedStart', type=int, help='Random seed start', default=1)
    parser.add_argument('--nEvtsPerNode', type=int, help='Number of events each node should process', default=50000)
    parser.add_argument('--totalNodes', type=int, help='Total number of nodes that should be used', default=2400)
    parser.add_argument('--sqrtS', type=int, help='Center of mass energy', default=13)
    parser.add_argument('--nEvts', type=int, help='Number of total events to produce', default=5000000)
    parser.add_argument('--workDirBase', type=str, help='Base working directory; could be local or shared', default="/usatlas/groups/bnl_local2/whopkins/deepMLSamps/")
    parser.add_argument('--basePath', type=str, help='Location where MadGraph is installed', default="/usatlas/u/whopkins/sampGen/")
    
    parser.add_argument('--rmLHE', action='store_true', help='Delete original LHE files to save space?', default=False)
    parser.add_argument('--rmDelphes', action='store_true', help='Delete original Delphes files to save space?', default=False)
    parser.add_argument('--skimDelphes', action='store_true', help='Skim and slim original Delphes files to save space?', default=False)
    parser.add_argument('--width', type=str, help='Widths of stop', default="")
    parser.add_argument('--preselec', type=str, help='Preselection', default="set ptj 20.0\nset pta 10.0\nset ptl 6.0\n\n")

    args = parser.parse_args()

    submitOneSamp(args.nEvts, args.process, nEvtsPerNode=args.nEvtsPerNode, totalNodes=args.totalNodes,
                  workDirBase=args.workDirBase, sqrtS=args.sqrtS, basePath=args.basePath, 
                  seedStart=args.seedStart, stopMass=args.stopMass, lspMass=args.lspMass, rmLHE=args.rmLHE, rmDelphes=args.rmDelphes,
                  skimDelphes=args.skimDelphes, preselection=args.preselec, width=args.width)
