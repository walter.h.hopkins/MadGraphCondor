#!/bin/env python

import os, argparse, sys, subprocess, getpass, time, math
import cPickle as pickle
from submitOneSamp import submitOneSamp

def getIdleJobs(user=getpass.getuser()):
    #condorCMD = "condor_q -submitter "+user+" -totals";
    condorCMD = "condor_q -totals";

    p = subprocess.Popen(condorCMD, shell=True, stdout=subprocess.PIPE, preexec_fn=os.setsid)
    (condorOutput, condorError) = p.communicate()
    condorELs = condorOutput.split(",")
    nIdleJobs = 0;
    for el in condorELs:
        if "idle" in el:
            nIdleJobs = int(el.split()[0])
        elif "running" in el:
            nRunningJobs = int(el.split()[0])
    return (nIdleJobs,nRunningJobs);

def limitMaxProcesses(maxProcesses, checkPs, checkOuts=None, checkFs=None):
    while len(checkPs.keys()) >= maxProcesses:
        # First look for processes that have finished and clean them up
        for process in checkPs.keys():
            if checkPs[process].poll() != None:
                output = checkPs[process].communicate();
                if checkOuts != None:
                    checkOuts[process] = output
                if checkFs != None:
                    checkFs[process][0].close();
                    checkFs[process][1].close();
                    checkFs.pop(process);
                checkPs.pop(process);

        # if the new process list is still long than the max wait for 20 seconds
        if len(checkPs.keys()) >= maxProcesses:
            time.sleep(5);
        
    return (checkPs, checkOuts, checkFs);

def submitScan(process, nEvts, nEvtsPerNode, totalNodes, workDirBase, sqrtSRange, basePath, finalOutDir, stopRange, lspRange, topRange, remoteDir, widths, mergeOnly=False):
    mergePs = {};
    mergeFs = {};
    maxProcesses = 8;
    nNodes = int(math.ceil((nEvts*1.0)/nEvtsPerNode))

    for topMass in topRange:
        for stopMass in stopRange:
            for lspMass in lspRange:
                for sqrtS in sqrtSRange:
                    (nIdleJobs, nRunningJobs) = getIdleJobs()
                    while not (nIdleJobs < 200 and float(nRunningJobs)/totalNodes < 0.4):
                        print "Waiting one minute before trying to submit jobs again because we have (totalNodes, idleJobs, runningJogs)", totalNodes, nIdleJobs, nRunningJobs
                        time.sleep(60)
                        (nIdleJobs, nRunningJobs) = getIdleJobs()

                    print "Submitting the jobs for", sqrtS, topMass, stopMass, lspMass
                    print workDirBase
                    print basePath
                    print finalOutDir

                    stopMassKey = "%.2f" % stopMass
                    if stopMassKey in widths.keys():
                        width = widths[stopMassKey]
                    else:
                        print stopMassKey
                        width = ''
                    idStr = submitOneSamp(nEvts, process, nEvtsPerNode=nEvtsPerNode, totalNodes=totalNodes, topMass=topMass, workDirBase=workDirBase, sqrtS=sqrtS, basePath=basePath, seedStart=1, stopMass=stopMass, lspMass=lspMass, preselection="", skimDelphes=False, rmDelphes=False, rmLHE=False, width=width, getIDStrOnly=mergeOnly)
                    outDir = workDirBase+"/submit/"+idStr
                    # if skimSlimDelphes:
                    #     outDir+='/skimSlim/'
                    # else:
                    outDir+='/delphes/'
                    remoteStr = ""
                    if remoteDir != "":
                        remoteStr = " --remoteDir "+remoteDir
                    cmd = "python mergeFiles.py "+outDir+" "+finalOutDir+idStr+".root "+str(nNodes)+remoteStr+" --delphesPath "+basePath+"/MG5_aMC_v2_6_1/Delphes/"
                    print cmd
                    if "GetWidth" not in process:
                        
                        mergeFs[idStr] = (open("mergeLogs/"+idStr+".out", "w"), open("mergeLogs/"+idStr+".err", "w"))
                        mergePs[idStr] = subprocess.Popen(cmd, shell=True, stdout=mergeFs[idStr][0], stderr=mergeFs[idStr][1], close_fds=True, preexec_fn=os.setsid)

                        if len(mergePs.keys()) >= maxProcesses:
                            print "Too many postprocesses running (NCurreent="+str(len(mergePs.keys()))+", NAllowed="+str(maxProcesses)+"). Waiting for 5 seconds..."
                            print mergePs.keys()
                            (mergePs, dummy, mergeFs) = limitMaxProcesses(maxProcesses, mergePs, checkFs=mergeFs);
                

                        



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Submit Madgraph jobs to condor')
    # Our scans have variable step size so the below won't work.
    parser.add_argument('process', type=str, help='Process to generate', default="stopDilep")
    parser.add_argument('--nEvtsPerNode', type=int, help='Number of events each node should process', default=50000)
    parser.add_argument('--totalNodes', type=int, help='Total number of nodes that should be used', default=2400)
    parser.add_argument('--minSqrtS', type=int, help='Minimum center of mass energy', default=13)
    parser.add_argument('--maxSqrtS', type=int, help='Maximum center of mass energy', default=13)
    parser.add_argument('--nEvts', type=int, help='Number of total events to produce', default=5000000)
    parser.add_argument('--basePath', type=str, help='Location where MadGraph is installed', default="/usatlas/u/whopkins/sampGen/")
    parser.add_argument('--finalOutDir', type=str, help='Where the merged delphes root file should be dumped', default="/usatlas/groups/bnl_local2/whopkins/deepMLSamps/samples/")
    parser.add_argument('--workDirBase', type=str, help='Base working directory; could be local or shared', default="/usatlas/groups/bnl_local2/whopkins/deepMLSamps/")
    parser.add_argument('--widthFile', type=str, help='File with pickled stop widths to use.', default="widths.pkl")
    parser.add_argument('--remoteDir', type=str, help='Remote location to copy final output file', default="")
    parser.add_argument('--mergeOnly', action='store_true', help='Only do the merge', default=False)
    
    args = parser.parse_args()

    #stopRange = [100+i*5 for i in range(21)]
    #lspRange = [0, 0.1, 1, 1000, 5000, 10000, 20000, 40000, 60000, 80000, 100000]
    
    stopRange = [600]
    lspRange = [300000]
    topRange = [172.5] # in GeV
    sqrtSRange = xrange(args.minSqrtS, args.maxSqrtS+1)
    print "stopRange =",stopRange
    print "lspRange =",lspRange
    print "topRange =",topRange
    print "Number of combination about to be processed:", len(stopRange)*len(lspRange)*len(topRange)*len(sqrtSRange)
    submitScan(args.process, args.nEvts, args.nEvtsPerNode, args.totalNodes, args.workDirBase, sqrtSRange,
               args.basePath, args.finalOutDir, stopRange, lspRange, topRange, args.remoteDir, widths=pickle.load(open(args.widthFile)),
               mergeOnly=args.mergeOnly)

