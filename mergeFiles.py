#!/bin/env python

import math, sys, glob, os, shutil, subprocess, time, argparse, signal
from ROOT import TFileMerger, TFile, TTree, gInterpreter, gSystem

def handler(signum, frame):
   print "Reached timeout for merge!"
   raise Exception("end of time")

def checkFiles(inDir):
   inFiles = []
   for inFName in glob.glob(inDir+"/*.root"):
      if os.stat(inFName).st_size > 0:
         inF = TFile.Open(inFName);
         if not inF or inF.TestBit(TFile.kRecovered):
            return []
         if inF.IsZombie():
            return None
         else:
            inFiles.append(inFName);
         inF.Close();
   return inFiles

def mergeFiles(watchDir, outPath, expNumFiles, remoteDir, delphesPath, timeOut=10800):
    TTree.SetMaxTreeSize( 1000000000000 );
    gInterpreter.AddIncludePath(delphesPath)
    gInterpreter.Declare('#include "'+delphesPath+'classes/DelphesClasses.h"')
    gSystem.Load(delphesPath+"libDelphes");

    signal.signal(signal.SIGALRM, handler);
    signal.alarm(timeOut)
    outFName = outPath
    if "/" in outPath:
        outFName = outPath.split('/')[-1]
    merger = TFileMerger(False)
    if os.path.exists(outPath):
        print "Removing file", outPath    
        os.unlink(outPath)

    print "Setting", outPath, "for merger"
    merger.OutputFile(outPath)
    
    processedFiles = []
    allGood = True;
    sleepTime = 60
    try: 
       while len(glob.glob(watchDir+"/*.root")) < expNumFiles or len(processedFiles) < expNumFiles:
            print "Number of processed files:", len(processedFiles), expNumFiles
            print "Checking files" 
            if processedFiles == checkFiles(watchDir):
                time.sleep(sleepTime);
                continue;
            newFiles = []
            #print "Checking files again"
            checkedFiles = checkFiles(watchDir)
            if checkedFiles == None:
                print "We have a zombie file. Not merging and breaking out of mergeFiles."
                allGood=False;
                break;
            for inFile in checkedFiles:
                if inFile not in processedFiles:
                   inF = TFile.Open(inFile)
                   names = [key.GetName() for key in inF.GetListOfKeys()]
                   inF.Close()
                   if "DelphesExt" in names:
                      merger.AddFile(inFile);
                      newFiles.append(inFile)
            if len(newFiles) > 0:

                allGood = allGood and merger.PartialMerge()
                processedFiles.extend(newFiles)
                print "Processed", len(processedFiles),"/", expNumFiles, "files"
    except Exception, exc:
       allGood = False
       print exc
        
    #Delete the input files
    if allGood:
       shutil.rmtree(watchDir);
        
    # Now copy the file to a remote destination if necessary
    if remoteDir != "" and allGood:
        cmd = "scp "+outPath+" "+remoteDir
        subprocess.Popen(cmd, shell=True)

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge files while waiting for job to finish')
    parser.add_argument("watchDir")
    parser.add_argument("outPath")
    parser.add_argument("expNumFiles", type=int)

    parser.add_argument('--remoteDir', type=str, help='Remote location to copy final output file', default="")
    parser.add_argument('--delphesPath', type=str, help='Path to delphes library.', default="")
    parser.add_argument('--timeOut', type=int, help='Timeout in seconds', default=10800)
    args = parser.parse_args()
  
    mergeFiles(args.watchDir, args.outPath, args.expNumFiles, args.remoteDir, args.delphesPath, timeOut=args.timeOut)
