#!/usr/bin/env python

from ROOT import TFile, TTree, TClonesArray, gROOT, gSystem, vector, TVector2, gInterpreter
from array import array
import os, argparse

def skimSlimAugment(inFPath, delphesPath, xsecFPath):
    TTree.SetMaxTreeSize( 1000000000000 );
    gInterpreter.AddIncludePath(delphesPath)
    gInterpreter.Declare('#include "'+delphesPath+'classes/DelphesClasses.h"')
    gSystem.Load(delphesPath+"libDelphes");

    with open(xsecFPath) as xsecFile:
        for line in xsecFile:
            if "Integrated weight" in line:
                xsec = float(line.split(":")[1])
    print xsec
    
    inF = TFile.Open(inFPath, "update")
    inTree = inF.Get("Delphes")
    count  = 0;
    nEntries = inTree.GetEntries()
    sumOfWeights = array('d', [0])
    xsecWeight = array('d', [xsec])

    for evt in inTree:
        sumOfWeights[0] += evt.Event[0].Weight
        if count % 100000 == 0:
            print "Processed", count, "of", nEntries, "which is", round(1.*count/nEntries*100), "percent"
        count+=1

    extTree = TTree("DelphesExt", "")
    extTree.AddFriend(inTree)
    extTree.Branch("SumOfWeights", sumOfWeights, "SumOfWeights/D")
    extTree.Branch("XSec", xsecWeight, "XSec/D")

    for evt in inTree:
        extTree.Fill()
    extTree.Write()
    inF.Close()
       
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Skim, slim, and augment delphes trees')
    parser.add_argument("inFPath")
    parser.add_argument("delphesPath")
    parser.add_argument("xsecFPath")
    args = parser.parse_args()
  
    skimSlimAugment(args.inFPath, args.delphesPath, args.xsecFPath)
