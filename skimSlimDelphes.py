#!/usr/bin/env python

from ROOT import TFile, TTree, TClonesArray, gROOT, gSystem, vector, TVector2, gInterpreter
from array import array
import os, argparse

def skimSlimAugment(inFPath, outFPath, delphesPath, xsecFPath):
    TTree.SetMaxTreeSize( 1000000000000 );
    gInterpreter.AddIncludePath(delphesPath)
    gInterpreter.Declare('#include "'+delphesPath+'classes/DelphesClasses.h"')
    gSystem.Load(delphesPath+"libDelphes");

    with open(xsecFPath) as xsecFile:
        for line in xsecFile:
            if "Integrated weight" in line:
                xsec = float(line.split(":")[1])
    print xsec
    inF = TFile.Open(inFPath)
    inTree = inF.Get("Delphes")
    
    outF = TFile.Open(outFPath, "RECREATE")
    newTree = TTree("DelphesSkimSlim", "")

    eventWeight = array('d', [0])
    nBJets = array('i', [0])
    nJets = array('i', [0])
    jetPt = vector(float)()
    jetEta = vector(float)()
    jetPhi = vector(float)()
    jetIsBTagged = vector(int)()
    
    met = array('f', [0])
    metPhi = array('f', [0])
    
    nTruthBJets = array('i', [0])
    nTruthJets = array('i', [0])
    truthJetPt = vector(float)()
    truthJetEta = vector(float)()
    truthJetPhi = vector(float)()
    
    metTruth = array('f', [0])
    metTruthPhi = array('f', [0])
    
    jetDPhiMetMin2 = array('f', [0])
    mtBMin = array('f', [0])
    mtBMax = array('f', [0])
    drbb = array('f', [0])

    weight = array('d', [xsec*1000])

    newTree.Branch("NBJets", nBJets, "NBJets/I")
    newTree.Branch("NJets", nJets, "NJets/I")
    newTree.Branch("JetPt", jetPt)
    newTree.Branch("JetEta", jetEta)
    newTree.Branch("JetPhi", jetPhi)
    newTree.Branch("JetIsBTagged", jetIsBTagged)

    newTree.Branch("NTruthBJets", nTruthBJets, "NTruthBJets/I")
    newTree.Branch("NTruthJets", nTruthJets, "NTruthJets/I")
    newTree.Branch("TruthJetPt", truthJetPt)
    newTree.Branch("TruthJetEta", truthJetEta)
    newTree.Branch("TruthJetPhi", truthJetPhi)
    
    newTree.Branch("Met", met, "Met/F")
    newTree.Branch("MetPhi", metPhi, "MetPhi/F")

    newTree.Branch("MetTruth", metTruth, "MetTruth/F")
    newTree.Branch("MetTruthPhi", metTruthPhi, "MetTruthPhi/F")
    
    newTree.Branch("JetDPhiMetMin2", jetDPhiMetMin2, "JetDPhiMetMin2/F")
    newTree.Branch("XSec", weight, "XSec/D")
    newTree.Branch("EventWeight", eventWeight, "EventWeight/D")
    count  = 0;
    nEntries = inTree.GetEntries()
    sumOfWeights = array('d', [0])
    for evt in inTree:
        sumOfWeights[0] += evt.Event[0].Weight
        if count % 100000 == 0:
            print "Processed", count, "of", nEntries, "which is", round(1.*count/nEntries*100), "percent"
        count+=1
        if evt.Electron_size > 0 or evt.Muon_size > 0 or evt.MissingET[0].MET<250 or evt.Jet_size<4 or evt.Jet[3].PT < 40:
            continue;
        nBJets[0] = 0
        nJets[0] = 0
        jetDPhiMetMin2[0] = 999;
        jetPt.clear()
        jetPhi.clear()
        jetEta.clear()
        jetIsBTagged.clear()
        jetCount = 0
        for jet in list(evt.Jet):
            if jet.PT < 20:
                continue
            jetPt.push_back(jet.PT)
            jetPhi.push_back(jet.Phi)
            jetEta.push_back(jet.Eta)
            jetIsBTagged.push_back(jet.BTag)
            nJets[0]+=1
            if jet.BTag:
                nBJets[0]+=1

            jetDPhiMet = TVector2.Phi_mpi_pi(jet.Phi-evt.MissingET[0].Phi)
            if nJets[0] < 2:
                if jetDPhiMet < jetDPhiMetMin2:
                    jetDPhiMetMin2[0] = jetDPhiMet
        if nBJets[0] < 1 or jetDPhiMetMin2[0] < 0.4:
            continue;

        nTruthBJets[0] = 0
        nTruthJets[0] = 0
        truthJetPt.clear()
        truthJetPhi.clear()
        truthJetEta.clear()
        truthJetCount = 0
        for truthJet in list(evt.GenJet):
            if truthJet.PT < 20:
                continue
            truthJetPt.push_back(truthJet.PT)
            truthJetPhi.push_back(truthJet.Phi)
            truthJetEta.push_back(truthJet.Eta)
            nTruthJets[0]+=1
            if truthJet.BTag:
                nTruthBJets[0]+=1
        
        met[0] = evt.MissingET[0].MET
        metPhi[0] = evt.MissingET[0].Phi
        
        metTruth[0] = evt.GenMissingET[0].MET
        metTruthPhi[0] = evt.GenMissingET[0].Phi

        eventWeight[0] = evt.Event[0].Weight
        newTree.Fill()

    newTree.Write()
    extTree = TTree("DelphesSkimSlimExt", "")
    extTree.AddFriend(newTree)
    extTree.Branch("SumOfWeights", sumOfWeights, "SumOfWeights/D")

    for evt in newTree:
        extTree.Fill()
    extTree.Write()
    inF.Close()
    outF.Close();
       
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Skim, slim, and augment delphes trees')
    parser.add_argument("inFPath")
    parser.add_argument("outFPath")
    parser.add_argument("delphesPath")
    parser.add_argument("xsecFPath")
    args = parser.parse_args()
  
    skimSlimAugment(args.inFPath, args.outFPath, args.delphesPath, args.xsecFPath)
